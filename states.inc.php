<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * noirkvi implementation : © CH Huang <chhuang76@gmail.com>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 * 
 * states.inc.php
 *
 * noirkvi game states description
 *
 */

/*
   Game state machine is a tool used to facilitate game developpement by doing common stuff that can be set up
   in a very easy way from this configuration file.

   Please check the BGA Studio presentation about game state to understand this, and associated documentation.

   Summary:

   States types:
   _ activeplayer: in this type of state, we expect some action from the active player.
   _ multipleactiveplayer: in this type of state, we expect some action from multiple players (the active players)
   _ game: this is an intermediary state where we don't expect any actions from players. Your game logic must decide what is the next game state.
   _ manager: special type for initial and final state

   Arguments of game states:
   _ name: the name of the GameState, in order you can recognize it on your own code.
   _ description: the description of the current game state is always displayed in the action status bar on
                  the top of the game. Most of the time this is useless for game state with "game" type.
   _ descriptionmyturn: the description of the current game state when it's your turn.
   _ type: defines the type of game states (activeplayer / multipleactiveplayer / game / manager)
   _ action: name of the method to call when this game state become the current game state. Usually, the
             action method is prefixed by "st" (ex: "stMyGameStateName").
   _ possibleactions: array that specify possible player actions on this step. It allows you to use "checkAction"
                      method on both client side (Javacript: this.checkAction) and server side (PHP: self::checkAction).
   _ transitions: the transitions are the possible paths to go from a game state to another. You must name
                  transitions in order to use transition names in "nextState" PHP method, and use IDs to
                  specify the next game state for each transition.
   _ args: name of the method to call to retrieve arguments for this gamestate. Arguments are sent to the
           client side to be used on "onEnteringState" or to set arguments in the gamestate description.
   _ updateGameProgression: when specified, the game progression is updated (=> call to your getGameProgression
                            method).
*/

//    !! It is not a good idea to modify this file when a game is running !!

$machinestates = array(

    // The initial state. Please do not modify.
    1 => array(
        "name" => "gameSetup",
        "description" => clienttranslate("Game setup"),
        "type" => "manager",
        "action" => "stGameSetup",
        "transitions" => array( "" => 11 )
    ),
    
    11 => array(
        "name" => "killerFirstTurn",
        "description" => clienttranslate('${actplayer} must kill a suspect'),
        "descriptionmyturn" => clienttranslate('${you} must kill a suspect'),
        "type" => "activeplayer",
        "possibleactions" => array( "kill", "zombiePass" ),
        "transitions" => array( "kill" => 12, "zombiePass" => 12 )
    ),

    12 => array(
        "name" => "afterFirstKill",
        "type" => "game",
        "action" => "stAfterFirstKill",
        "updateGameProgression" => true,        
        "transitions" => array( "nextTurn" => 13 )
    ),

    13 => array(
        "name" => "inspectorFirstTurn",
        "description" => clienttranslate('${actplayer} must pick an identity'),
        "descriptionmyturn" => clienttranslate('${you} must pick an identity'),
        "type" => "activeplayer",
        "possibleactions" => array( "pickID" ),
        "transitions" => array( "pickID" => 14 )
    ),

    14 => array(
        "name" => "afterPickID",
        "type" => "game",
        "action" => "stAfterPickID",
        "updateGameProgression" => true,        
        "transitions" => array( "nextTurn" => 2 )
    ),

    // Regular turn
    2 => array(
        "name" => "killerTurn",
        "description" => clienttranslate('${actplayer} must perform an action'),
        "descriptionmyturn" => clienttranslate('${you} must perform an action'),
        "type" => "activeplayer",
        "possibleactions" => array( "shift", "kill", "disguise", "zombiePass" ),
        "transitions" => array( "shift" => 21, "kill" => 21, "disguise" => 21, "zombiePass" => 21)
    ),

    21 => array(
        "name" => "afterKiller",
        "type" => "game",
        "action" => "stAfterKiller",
        "updateGameProgression" => true,
        "transitions" => array( "nextTurn" => 3,  "endGame" => 99 )
    ),

    3 => array(
        "name" => "inspectorTurn",
        "description" => clienttranslate('${actplayer} must perform an action'),
        "descriptionmyturn" => clienttranslate('${you} must perform an action'),
        "type" => "activeplayer",
        "possibleactions" => array( "shift", "arrest", "exonerate", "citizensArrest", "zombiePass" ),
        "transitions" => array( "shift" => 32, "arrest" => 32, "exonerate" => 31,
          "citizensArrest" => 33, "zombiePass" => 32)
    ),

    31 => array(
        "name" => "inspectorPickExonerate",
        "description" => clienttranslate('${actplayer} must discard a card'),
        "descriptionmyturn" => clienttranslate('${you} must discard a card from your hand'),
        "type" => "activeplayer",
        "possibleactions" => array( "pickExonerate" ),
        "transitions" => array( "pickExonerate" => 32 )
    ),

    33 => array(
        "name" => "inspectorPickCitizensArrestCard",
        "description" => clienttranslate('${actplayer} must discard a card'),
        "descriptionmyturn" => clienttranslate('${you} must discard a card from your hand.'),
        "type" => "activeplayer",
        "possibleactions" => array( "pickCitizensArrestCard", "pickCitizensArrestCardInvalid" ),
        "transitions" => array( "pickCitizensArrestCard" => 34, "pickCitizensArrestCardInvalid" => 3 )
    ),

    34 => array(
        "name" => "inspectorPickCitizensArrestTarget",
        "description" => clienttranslate('${actplayer} must pick a suspect'),
        "descriptionmyturn" => clienttranslate('${you} must pick a suspect adjacent to newly exonerated character.'),
        "type" => "activeplayer",
        "possibleactions" => array( "pickCitizensArrestTarget" ),
        "transitions" => array( "pickCitizensArrestTarget" => 32 )
    ),

    32 => array(
        "name" => "afterInspector",
        "type" => "game",
        "action" => "stAfterInspector",
        "updateGameProgression" => true,
        "transitions" => array( "nextTurn" => 2,  "endGame" => 99 )
    ),

    // Final state.
    // Please do not modify.
    99 => array(
        "name" => "gameEnd",
        "description" => clienttranslate("End of game"),
        "type" => "manager",
        "action" => "stGameEnd",
        "args" => "argGameEnd"
    )
);
