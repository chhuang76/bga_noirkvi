<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * noirkvi implementation : © CH Huang <chhuang76@gmail.com>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * gameoptions.inc.php
 *
 * noirkvi game options description
 * 
 * In this file, you can define your game options (= game variants).
 *   
 * Note: If your game has no variant, you don't have to modify this file.
 *
 * Note²: All options defined in this file should have a corresponding "game state labels"
 *        with the same ID (see "initGameStateLabels" in noirkvi.game.php)
 *
 * !! It is not a good idea to modify this file when a game is running !!
 *
 */

$game_options = array(
    100 => array(
            'name' => totranslate('Variant'),
            'values' => array(
                    3 => array( 'name' => totranslate( "Citizen's Arrest" )),
                    1 => array( 'name' => totranslate( 'Standard' )),
                    2 => array( 'name' => totranslate( 'Classic' )),
            ),
            'default' => 3
    ),
    101 => array(
            'name' => totranslate('Roles'),
            'values' => array(
                    1 => array( 'name' => totranslate( 'Random' ) ),
                    //2 => array( 'name' => totranslate( 'First player is the killer' )),
                    3 => array( 'name' => totranslate( 'First player is the inspector' )),
            ),
            'default' => 1,
            'resetOnAdminChange' => true
    ),
);


