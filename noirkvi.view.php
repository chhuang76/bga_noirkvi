<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * noirkvi implementation : © CH Huang <chhuang76@gmail.com>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * noirkvi.view.php
 *
 * This is your "view" file.
 *
 * The method "build_page" below is called each time the game interface is displayed to a player, ie:
 * _ when the game starts
 * _ when a player refreshes the game page (F5)
 *
 * "build_page" method allows you to dynamically modify the HTML generated for the game interface. In
 * particular, you can set here the values of variables elements defined in noirkvi_noirkvi.tpl (elements
 * like {MY_VARIABLE_ELEMENT}), and insert HTML block elements (also defined in your HTML template file)
 *
 * Note: if the HTML of your game interface is always the same, you don't have to place anything here.
 *
 */
  
  require_once( APP_BASE_PATH."view/common/game.view.php" );
  
  class view_noirkvi_noirkvi extends game_view
  {
    function getGameName() {
        return "noirkvi";
    }
    function build_page( $viewArgs )
    {
        // Get players & players number
        $players = $this->game->loadPlayersBasicInfos();
        $players_nbr = count( $players );

        /*********** Place your code below:  ************/
        $this->page->begin_block( "noirkvi_noirkvi", "suspectCard" );

        $width = 72;
        $height = 96;
        $margin = 0;
        $arrowWidth = 48;
        for( $x=0; $x<5; $x++ ){
            for( $y=0; $y<5; $y++ ){
                $this->page->insert_block( "suspectCard", array(
                    'X' => $x,
                    'Y' => $y,
                    'LEFT' => round( $x*$width + $margin + $arrowWidth),
                    'TOP' => round( $y*$height + $margin + $arrowWidth)
                ) );
            }
        }

        $this->tpl['Disguise'] = self::_("Disguise");
        $this->tpl['Exonerate'] = self::_("Exonerate");
        $this->tpl["Citizen's Arrest"] = self::_("Citizen's Arrest");
        /*********** Do not change anything below this line  ************/
    }
  }


