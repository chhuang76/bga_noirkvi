{OVERALL_GAME_HEADER}

<!-- 
--------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- noirkvi implementation : © CH Huang <chhuang76@gmail.com>
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-------
-->
<div id="gameboardContainer">
    <div id="gameboard">
        <div id="shiftArrow_Up_0" class="shiftArrow shiftArrow_Up" style="left: 60px;"></div>
        <div id="shiftArrow_Up_1" class="shiftArrow shiftArrow_Up" style="left: 132px;"></div>
        <div id="shiftArrow_Up_2" class="shiftArrow shiftArrow_Up" style="left: 204px;"></div>
        <div id="shiftArrow_Up_3" class="shiftArrow shiftArrow_Up" style="left: 276px;"></div>
        <div id="shiftArrow_Up_4" class="shiftArrow shiftArrow_Up" style="left: 348px;"></div>

        <div id="shiftArrow_Left_0" class="shiftArrow shiftArrow_Left" style="top: 72px;"></div>
        <div id="shiftArrow_Left_1" class="shiftArrow shiftArrow_Left" style="top: 168px;"></div>
        <div id="shiftArrow_Left_2" class="shiftArrow shiftArrow_Left" style="top: 264px;"></div>
        <div id="shiftArrow_Left_3" class="shiftArrow shiftArrow_Left" style="top: 360px;"></div>
        <div id="shiftArrow_Left_4" class="shiftArrow shiftArrow_Left" style="top: 456px;"></div>

        <div id="shiftArrow_Right_0" class="shiftArrow shiftArrow_Right" style="left: 408px; top: 72px;"></div>
        <div id="shiftArrow_Right_1" class="shiftArrow shiftArrow_Right" style="left: 408px; top: 168px;"></div>
        <div id="shiftArrow_Right_2" class="shiftArrow shiftArrow_Right" style="left: 408px; top: 264px;"></div>
        <div id="shiftArrow_Right_3" class="shiftArrow shiftArrow_Right" style="left: 408px; top: 360px;"></div>
        <div id="shiftArrow_Right_4" class="shiftArrow shiftArrow_Right" style="left: 408px; top: 456px;"></div>

        <div id="shiftArrow_Down_0" class="shiftArrow shiftArrow_Down" style="left: 60px; top: 528px;"></div>
        <div id="shiftArrow_Down_1" class="shiftArrow shiftArrow_Down" style="left: 132px; top: 528px;"></div>
        <div id="shiftArrow_Down_2" class="shiftArrow shiftArrow_Down" style="left: 204px; top: 528px;"></div>
        <div id="shiftArrow_Down_3" class="shiftArrow shiftArrow_Down" style="left: 276px; top: 528px;"></div>
        <div id="shiftArrow_Down_4" class="shiftArrow shiftArrow_Down" style="left: 348px; top: 528px;"></div>

        <!-- BEGIN suspectCard -->
            <div id="suspectCard_{X}_{Y}" class="suspectCard" style="left: {LEFT}px; top: {TOP}px;">
                <div id="killedHighLight_{X}_{Y}" class="killedHighLight"></div>
                <div id="innocentHighLight_{X}_{Y}" class="innocentHighLight"></div>
                <div id="idHighLight_{X}_{Y}" class="idHighLight"></div>
                <div id="eviHighLight_{X}_{Y}" class="eviHighLight"></div>
                <div id="arrestHighLight_{X}_{Y}" class="arrestHighLight"></div>
                <div id="alertHighLight_{X}_{Y}" class="alertHighLight"></div>
                <div id="questionHighLight_{X}_{Y}" class="questionHighLight"></div>
            </div>
        <!-- END suspectCard -->

        <h5 id="numCharactersDeceased" class="whiteblock"></h5>
    </div>


    <div id="deckAndIdContainer">
        <div id="myRole" class="whiteblock"></div>

        <div id="eviDeckContainer" class="whiteblock">
            <div id="eviDeckBg">
                <div id="eviDeck"></div> 
            </div>
            </br>
            <a href="#" id="disguise" class="button" style="margin-left:auto; margin-right: auto;"><span>{Disguise}</span></a>
            <a href="#" id="exonerate" class="button" style="margin-left:auto; margin-right: auto;"><span>{Exonerate}</span></a>
        </div>

        <div id="myIdContainer" class="whiteblock">
            <h5 id="myID"></h5>
            <div id="myIDcard" style="float: none"></div>
        </div>

        <!--/br-->

        <div id="inspectorsHand" class="whiteblock">
            <h5 id="eviCardsInYourHand"></h5>
            <div id="inspectorsEviCard_0" class="pickID"></div>
            <div id="inspectorsEviCard_1" class="pickID"></div>
            <div id="inspectorsEviCard_2" class="pickID"></div>
            <div id="inspectorsEviCard_3" class="pickID"></div>
            </br>
            <a href="#" id="citizensArrest" class="button" style="margin-left:auto; margin-right: auto;"><span>{Citizen's Arrest}</span></a>
        </div>
    </div>

</div>

<script type="text/javascript">

</script>  

{OVERALL_GAME_FOOTER}
