<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * Noirkvi implementation : © CH Huang <chhuang76@gmail.com>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 * 
 *
 */
  
  
class action_noirkvi extends APP_GameAction
{ 
    // Constructor: please do not modify
    public function __default()
    {
        if( self::isArg( 'notifwindow') )
        {
            $this->view = "common_notifwindow";
            $this->viewArgs['table'] = self::getArg( "table", AT_posint, true );
        }
        else
        {
            $this->view = "noirkvi_noirkvi";
            self::trace( "Complete reinitialization of board game" );
      }
    } 
    
    public function pickID() {
        self::setAjaxMode();
        $value = self::getArg( "id", AT_alphanum, true );
        $result = $this->game->pickID($value);
        self::ajaxResponse( );
    }

    public function shift() {
        self::setAjaxMode();
        $type = self::getArg( "type", AT_alphanum, true );
        $index = self::getArg( "index", AT_int, true );
        $result = $this->game->shift($type, $index);
        self::ajaxResponse( );
    }

    public function kill () {
        self::setAjaxMode();
        $value = self::getArg( "id", AT_alphanum, true );
        $result = $this->game->kill($value);
        self::ajaxResponse( );
    }

    public function disguise () {
        self::setAjaxMode();
        $result = $this->game->disguise();
        self::ajaxResponse( );
    }

    public function arrest () {
        self::setAjaxMode();
        $value = self::getArg( "id", AT_alphanum, true );
        $result = $this->game->arrest($value);
        self::ajaxResponse( );
    }

    public function exonerate () {
        self::setAjaxMode();
        $result = $this->game->exonerate();
        self::ajaxResponse( );
    }

    public function pickExonerate () {
        self::setAjaxMode();
        $value = self::getArg( "id", AT_alphanum, true );
        $result = $this->game->pickExonerate($value);
        self::ajaxResponse( );
    }

    public function citizensArrest () {
        self::setAjaxMode();
        $result = $this->game->citizensArrest();
        self::ajaxResponse( );
    }

    public function pickCitizensArrestCard () {
        self::setAjaxMode();
        $value = self::getArg( "id", AT_alphanum, true );
        $result = $this->game->pickCitizensArrestCard($value);
        self::ajaxResponse( );
    }

    public function pickCitizensArrestTarget () {
        self::setAjaxMode();
        $value = self::getArg( "id", AT_alphanum, true );
        $result = $this->game->pickCitizensArrestTarget($value);
        self::ajaxResponse( );
    }
}


