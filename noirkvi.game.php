<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * noirkvi implementation : © CH Huang <chhuang76@gmail.com>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * noirkvi.game.php
 *
 * This is the main file for your game logic.
 *
 * In this PHP file, you are going to defines the rules of the game.
 *
 */
require_once (APP_GAMEMODULE_PATH . 'module/table/table.game.php');
class noirkvi extends Table {
    private static $charName = array(
	'a' => 'Alyss', 'b' => 'Barrin', 'c' => 'Clive', 'd' => 'Deidre', 
	'e' => 'Ernest', 'f' => 'Franklin', 'g' => 'Geneva', 'h' => 'Horatio', 
	'i' => 'Irma', 'j' => 'Julian', 'k' => 'Kristoph', 'l' => 'Linus', 
	'm' => 'Marion', 'n' => 'Niel', 'o' => 'Ophelia', 'p' => 'Phoebe', 
	'q' => 'Quinton', 'r' => 'Ryan', 's' => 'Simon', 't' => 'Trevor', 
	'u' => 'Udstad', 'v' => 'Vladmir', 'w' => 'Wilhelm', 'x' => 'Zachary', 
	'y' => 'Yvonne');
    function __construct() {
        // Your global variables labels:
        //  Here, you can assign labels to global variables you are using for this game.
        //  You can use any number of global variables with IDs between 10 and 99.
        //  If your game has options (variants), you also have to associate here a label to
        //  the corresponding ID in gameoptions.inc.php.
        // Note: afterwards, you can get/set the global variables with getGameStateValue/setGameStateInitialValue/setGameStateValue
        parent::__construct();
        self::initGameStateLabels(
	    array("killer_bga_playerID" => 10, "inspector_bga_playerID" => 11, 
		  "gameVarient" => 100, "playerRolesVarient" => 101,)
	);
    }
    protected function getGameName() {
        return "noirkvi";
    }
    /*
        setupNewGame:
        
        This method is called only once, when a new game is launched.
        In this method, you must setup the game according to the game rules, so that
        the game is ready to be played.
    */
    protected function setupNewGame($players, $options = array()) {
        $sql = "DELETE FROM player WHERE 1 ";
        self::DbQuery($sql);
        // Set the colors of the players with HTML color code
        // The default below is red/green/blue/orange/brown
        // The number of colors defined here must correspond to the maximum number of players allowed for the gams
        $coinFlip = rand() % 2;
        $varient = self::getGameStateValue('playerRolesVarient');
        if ($varient == 2) {
            $coinFlip = 0;
        } else if ($varient == 3) {
            $coinFlip = 1;
        }
        if ($coinFlip == 0) {
            $default_colors = array("000000", "ffffff");
        } else {
            $default_colors = array("ffffff", "000000");
        }
        // Create players
        // Note: if you added some extra field on "player" table in the database (dbmodel.sql), you can initialize it there.
        $this->moveAdminPlayerIDtoFirst($players);
        $sql = "INSERT INTO player (player_id, player_color, player_canal, player_name, player_avatar) VALUES ";
        $values = array();
        foreach ($players as $player_id => $player) {
            $color = array_shift($default_colors);
            $values[] = "('" . $player_id . "','$color','" . $player['player_canal'] . "','" . addslashes($player['player_name']) . "','" . addslashes($player['player_avatar']) . "')";
            if ($color == "000000") {
                self::setGameStateInitialValue('killer_bga_playerID', $player_id);
            } else {
                self::setGameStateInitialValue('inspector_bga_playerID', $player_id);
            }
        }
        $sql.= implode($values, ',');
        self::DbQuery($sql);
        self::reloadPlayersBasicInfos();
        /************ Start the game initialization *****/
        // Init global values with their initial values
        //self::setGameStateInitialValue( 'my_first_global_variable', 0 );
        // Init game statistics
        // (note: statistics used in this file must be defined in your stats.inc.php file)
        self::initStat('table', 'remainingCards', 0);
        self::initStat('player', 'suspectsKilled', 0);
        self::initStat('player', 'suspectsExonerated', 0);
        self::initStat('player', 'shiftAction', 0);
        self::initStat('player', 'killAction', 0);
        self::initStat('player', 'disguiseAction', 0);
        self::initStat('player', 'arrestAction', 0);
        self::initStat('player', 'exonerateAction', 0);

        $this->initGameBoard();
        // Activate first player (which is in general a good idea :) )
        $this->gamestate->changeActivePlayer(self::getGameStateValue('killer_bga_playerID'));
        /************ End of the game initialization *****/
    }
    function moveAdminPlayerIDtoFirst(&$players) {
        $adminID = $this->getAdminPlayerID();
        if ($adminID != 0) {
            $adminValue = $players[$adminID];
            if (($key = array_search($adminID, $players)) !== false) {
                unset($players[$key]);
            }
            $players = array($adminID => $adminValue) + $players;
        }
    }
    function getAdminPlayerID() {
        return self::getUniqueValueFromDB("SELECT global_value value FROM global WHERE global_id = 5");
    }
    function initGameBoard() {
        $eviDeck = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
			 'w', 'x', 'y');
        $suspectDeck = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			     'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
			     'u', 'v', 'w', 'x', 'y');
        shuffle($eviDeck);
        shuffle($suspectDeck);
        $sqlFormat = "INSERT INTO board (gameBoard, eviDeck, kID, iHand) VALUES ('%s', '%s', '%s', '%s')";
        $kID = array_shift($eviDeck);
        $iHand = array_shift($eviDeck) . array_shift($eviDeck) . array_shift($eviDeck) . array_shift($eviDeck);
        $deckString = implode('', $eviDeck);
        self::DbQuery(sprintf($sqlFormat, '55' . implode('0', $suspectDeck) . '0', $deckString, $kID, $iHand));
        self::setStat(strlen($deckString), 'remainingCards');
    }
    /*
        getAllDatas: 
        
        Gather all informations about current game situation (visible by the current player).
        
        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas() {
        $result = array('players' => array());
        $current_player_id = self::getCurrentPlayerId(); // !! We must only return informations visible by this player !!
        // Get information about players
        // Note: you can retrieve some extra field you added for "player" table in "dbmodel.sql" if you need it.
        $sql = "SELECT player_id id, player_score score FROM player ";
        $result['players'] = self::getCollectionFromDb($sql);
        $result['myRole'] = self::getUniqueValueFromDB(
            "SELECT player_color roleColor FROM player WHERE player_id = $current_player_id");
        if (strcmp($result['myRole'], 'ffffff') == 0) {
            $result['gameBoard'] = self::getObjectFromDB(
                "SELECT gameBoard gameBoard, iID id, iHand hand, forbiddenShift forbiddenShift, lastExonerated lastExonerated FROM board LIMIT 1");
        } else {
            $result['gameBoard'] = self::getObjectFromDB(
                "SELECT gameBoard gameBoard, kID id, forbiddenShift forbiddenShift, lastExonerated lastExonerated FROM board LIMIT 1");
        }
        $result['remainingDeckSize'] = strlen(self::getUniqueValueFromDB("SELECT eviDeck deck FROM board LIMIT 1"));
        $result['gameMode'] = self::getGameStateValue('gameVarient');
        return $result;
    }
    /*
        getGameProgression:
        
        Compute and return the current game progression.
        The number returned must be an integer beween 0 (=the game just started) and
        100 (= the game is finished or almost finished).
    
        This method is called each time we are in a game state with the "updateGameProgression" property set to true 
        (see states.inc.php)
    */
    function getGameProgression() {
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard, iID iID, kID kID FROM board LIMIT 1");
        $board = $gameData['gameBoard'];
        $state = $board[strpos($board, $gameData['iID']) + 1];
        if (($state % 2) == 1) {
            return 100; //inspector is killed
        }
        $state = $board[strpos($board, $gameData['kID']) + 1];
        if ($state >= 4) {
            return 100; //killer is arrested
        }
        $killed = $this->countKilled($board);
        return $killed * 100 / 16;
    }
    //////////////////////////////////////////////////////////////////////////////
    //////////// Utility functions
    ////////////
    function isAdvanced() {
        return self::getGameStateValue('gameVarient') == 1;
    }
    function isClassic() {
        return self::getGameStateValue('gameVarient') == 2;
    }
    function isCitizenArrest() {
        return self::getGameStateValue('gameVarient') == 3;
    }
    function shiftRow($board, $r, $mov) {
        $w = $board[0];
        $h = $board[1];
        $newHead = (-1 * $mov) % $w;
        $targetRowStr = substr($board, 2 + $r * 10, $w * 2);
        $newRowStr = '';
        for ($i = 0;$i < $w;$i++) {
            $newRowStr.= substr($targetRowStr, ($i + $newHead) * 2 % ($w * 2), 2);
        }
        $newRowStr.= substr($targetRowStr, strlen($newRowStr));
        $newBoardHead = substr($board, 0, 2 + $r * 10);
        $newBoard = $newBoardHead . $newRowStr;
        $newBoard.= substr($board, strlen($newBoard));
        return $newBoard;
    }
    function shiftCol($board, $c, $mov) {
        $flippedBoard = $this->boardColToRow($board);
        $afterShift = $this->shiftRow($flippedBoard, $c, $mov);
        return $this->boardColToRow($afterShift);
    }
    function boardColToRow($board) {
        $w = $board[0];
        $h = $board[1];
        $boardArray = str_split(substr($board, 2), 2);
        $this->arrayColToRow($boardArray);
        return $h . $w . implode('', $boardArray);
    }
    function arrayColToRow(&$array) {
        for ($i = 0;$i < 5;$i++) {
            for ($j = $i;$j < 5;$j++) {
                $tmp = $array[$i * 5 + $j];
                $array[$i * 5 + $j] = $array[$j * 5 + $i];
                $array[$j * 5 + $i] = $tmp;
            }
        }
    }
    function shrinkBoard(&$board) //update the board and return shrink detail
    {
        $shrinkDetailArray = array();
        $shrink = $this->shrinkRows($board, $shrinkDetailArray);
        $shrinkDetailStr = '';
        if (count($shrinkDetailArray) > 0) {
            $shrinkDetailStr = 'r' . implode('r', $shrinkDetailArray);
        }
        $flipped = $this->boardColToRow($shrink);
        $shrinkDetailArray = array();
        $shrink = $this->shrinkRows($flipped, $shrinkDetailArray);
        if (count($shrinkDetailArray) > 0) {
            $shrinkDetailStr.= 'c' . implode('c', $shrinkDetailArray);
        }
        $board = $this->boardColToRow($shrink);
        return $shrinkDetailStr;
    }
    function shrinkRows($board, &$shrinkDetail = null) {
        $w = $board[0];
        $h = $board[1];
        $head = '';
        $tail = '';
        $shrinkCount = 0;
        for ($y = 0;$y < $h;$y++) {
            $toShrink = true;
            for ($x = 0;$x < $w;$x++) {
                if (($board[2 + $y * 10 + $x * 2 + 1] % 2) != 1) {
                    $toShrink = false;
                    break;
                }
            }
            if ($toShrink) {
                $tail.= substr($board, 2 + $y * 10, 10);
                $shrinkCount++;
                if (is_array($shrinkDetail)) {
                    array_push($shrinkDetail, $y);
                }
            } else {
                $head.= substr($board, 2 + $y * 10, 10);
            }
        }
        $tail.= substr($board, 2 + $h * 10);
        return $w . ($h - $shrinkCount) . $head . $tail;
    }
    function notifyBoardShrink($shrinkDetailStr) {
        if (strpos($shrinkDetailStr, 'r') !== false) {
            self::notifyAllPlayers("doNothing",
                clienttranslate('An entire row of suspects are deceased, that row is removed.'), array());
        }
        if (strpos($shrinkDetailStr, 'c') !== false) {
            self::notifyAllPlayers("doNothing",
                clienttranslate('An entire column of suspects are deceased, that column is removed.'), array());
        }
    }
    function isTwoCharacterAdjacent($charA, $charB, $board) {
        $w = $board[0];
        $h = $board[1];
        $posA = (strpos($board, $charA) - 2) / 2;
        $xA = $posA % 5;
        $yA = floor($posA / 5);
        # if char A is not on the game board, return false
        if (($xA + 1 > $w) || ($yA + 1 > $h)) {
            return false;
        }
        $posB = (strpos($board, $charB) - 2) / 2;
        $xB = $posB % 5;
        $yB = floor($posB / 5);
        # if char B is not on the game board, return false
        if (($xB + 1 > $w) || ($yB + 1 > $h)) {
            return false;
        }
        if (abs($xA - $xB) > 1 || abs($yA - $yB) > 1) {
            return false;
        }
        return true;
    }
    function getAllAdjacentCharacters($char, $board) {
        // TODO: optimize this
        $allChars = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
             'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
             'w', 'x', 'y');
        $adjacentChars = array();
        foreach ($allChars as $targetChar) {
            if ($targetChar == $char) {
                continue;
            }
            else if ($this->isTwoCharacterAdjacent($char, $targetChar, $board)) {
                array_push($adjacentChars, $targetChar);
            }
        }
        return $adjacentChars;
    }
    function isCharacterOnGameBoard($char, $board) {
        $w = $board[0];
        $h = $board[1];
        $pos = (strpos($board, $char) - 2) / 2;
        $x = $pos % 5;
        $y = floor($pos / 5);
        if (($x + 1 > $w) || ($y + 1 > $h)) {
            return false;
        }
        return true;
    }
    function isKillled($state) {
        return ($state % 2) == 1;
    }
    function isExonerated($state) {
        return ($state & 2) > 0;
    }
    function canCharacterPerformArrest($char, $board) {
        // check if character can perform arrests action before Citizen's Arrest
        // (it must has an adjacent character which is not killed and not exonerated)
        $adjacentChars = $this->getAllAdjacentCharacters($char, $board);
        foreach ($adjacentChars as $c) {
            $index = strpos($board, $c);
            $state = $board[$index + 1];
            if (!$this->isKillled($state) && !$this->isExonerated($state)) {
                return true;
            }
        }
        return false;
    }
    //////////////////////////////////////////////////////////////////////////////
    //////////// Player actions
    ////////////
    public function pickID($value) {
        self::checkAction('pickID');
        $current_player_id = self::getCurrentPlayerId();
        $allDatas = $this->getAllDatas();
        $sqlFormat = "UPDATE board SET iID='%s', iHand='%s' LIMIT 1";
        $iID = $value;
        $iHand = implode('', explode($iID, $allDatas['gameBoard']['hand']));
        self::DbQuery(sprintf($sqlFormat, $iID, $iHand));
        self::notifyPlayer($current_player_id, "updateID", "", array('newID' => $iID));
        self::notifyPlayer($current_player_id, "updateHand", "", array('iHand' => $iHand));
        self::notifyAllPlayers("pickID",
            clienttranslate('${player_name} picks an identity'),
            array('player_name' => self::getActivePlayerName()));
        $this->gamestate->nextState('pickID');
    }
    public function shift($type, $index) {
        self::checkAction('shift');
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard FROM board LIMIT 1");
        $newboard = $gameData['gameBoard'];
        $shiftType = '';
        switch (strtolower($type)) {
            case 'up':
                $newboard = $this->shiftCol($newboard, $index, -1);
                $shiftType = clienttranslate('column');
            break;
            case 'down':
                $newboard = $this->shiftCol($newboard, $index, 1);
                $shiftType = clienttranslate('column');
            break;
            case 'left':
                $newboard = $this->shiftRow($newboard, $index, -1);
                $shiftType = clienttranslate('row');
            break;
            case 'right':
                $newboard = $this->shiftRow($newboard, $index, 1);
                $shiftType = clienttranslate('row');
            break;
        }
        $beforeShrink = $newboard;
        $shrinkDetail = $this->shrinkBoard($newboard);
        $forbiddenShift = '';
        if (strcmp($beforeShrink, $newboard) == 0) {
            $forbiddenShift = $this->reverseShiftType($type) . '_' . $index;
            $this->saveBoard($newboard, $forbiddenShift);
        } else {
            $this->saveBoard($newboard);
        }
        self::incStat(1, "shiftAction", self::getActivePlayerId());
        self::notifyAllPlayers("shift",
            clienttranslate('${player_name} shifts a ${shiftType}'),
            array('player_name' => self::getActivePlayerName(),
                'shiftType' => $shiftType,
                'newBoard' => $newboard,
                'beforeShrink' => $beforeShrink,
                'shrinkDetail' => $shrinkDetail,
                'forbiddenShift' => $forbiddenShift,
                'i18n' => array('shiftType')));
        $this->notifyBoardShrink($shrinkDetail);
        $this->gamestate->nextState('shift');
    }
    private function reverseShiftType($type) {
        switch (strtolower($type)) {
            case 'up':
                return 'Down';
            case 'down':
                return 'Up';
            case 'left':
                return 'Right';
            case 'right':
                return 'Left';
        }
    }
    public function kill($value) {
        if ($this->isClassic()) {
            $this->classicKill($value);
        } else {
            $this->advancedKill($value);
        }
    }
    public function classicKill($value) {
        self::checkAction('kill');
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard FROM board LIMIT 1");
        $index = strpos($gameData['gameBoard'], $value);
        $oldState = $gameData['gameBoard'][$index + 1];
        if ($this->isKillled($oldState)) {
            //target is already dead. Something went wrong.
        } else {
            $newboard = $gameData['gameBoard'];
            $newboard[$index + 1] = $oldState | 1;
            $beforeShrink = $newboard;
            $shrinkDetail = $this->shrinkBoard($newboard);
            $this->saveBoard($newboard);
            self::incStat(1, "killAction", self::getGameStateValue('killer_bga_playerID'));
            self::notifyAllPlayers("kill",
                clienttranslate('${player_name} kills ${target}'),
                array('player_name' => self::getActivePlayerName(),
                    'target' => self::$charName[$value],
                    'newBoard' => $newboard,
                    'beforeShrink' => $beforeShrink,
                    'shrinkDetail' => $shrinkDetail));
            $this->notifyBoardShrink($shrinkDetail);
            $this->gamestate->nextState('kill');
        }
    }
    public function advancedKill($value) {
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard, iID iID FROM board LIMIT 1");
        $index = strpos($gameData['gameBoard'], $value);
        $oldState = $gameData['gameBoard'][$index + 1];
        $this->classicKill($value);
        if ($this->isExonerated($oldState)) {
            if ($this->isTwoCharacterAdjacent($value, $gameData['iID'], $gameData['gameBoard'])) {
                self::notifyAllPlayers("canvasCharacter",
                    clienttranslate('The inspector is adjacent to ${target}'),
                    array('target' => self::$charName[$value],
                        'targetId' => $value, 'isAlert' => true));
            } else {
                self::notifyAllPlayers("canvasCharacter",
                    clienttranslate('The inspector is not adjacent to ${target}'),
                    array('target' => self::$charName[$value],
                        'targetId' => $value, 'isAlert' => false));
            }
        }
    }
    public function disguise() {
        self::checkAction('disguise');
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard, eviDeck deck, kID kID FROM board LIMIT 1");
        if (strlen($gameData['deck']) < 1) {
            // something went wrong!
        } else {
            $card = $gameData['deck'][0];
            $newDeck = substr($gameData['deck'], 1);
            $newboard = $gameData['gameBoard'];
            $index = strpos($newboard, $card);
            if ($index === FALSE) {
                // something went wrong!
            } else {
                $oldState = $newboard[$index + 1];
                self::incStat(1, "disguiseAction", self::getGameStateValue('killer_bga_playerID'));
                if ($this->isKillled($oldState)) {
                    //Target is dead, disguising failed
                    $this->saveDeck($newDeck);
                    self::notifyAllPlayers("disguise",
                        clienttranslate('${player_name} fails to disguise. ${target} is deceased.'),
                        array('player_name' => self::getActivePlayerName(),
                            'target' => self::$charName[$card],
                            'newBoard' => $newboard, 'remainingDeckSize' => strlen($newDeck),
                            'beforeShrink' => $newboard, 'shrinkDetail' => ''));
                    $this->gamestate->nextState('disguise');
                } else {
                    //Kill/Exonerate kID on board, update kID to $card
                    $index = strpos($newboard, $gameData['kID']);
                    $oldState = $newboard[$index + 1];
                    $newState = $oldState | 1;
                    $action = clienttranslate('kills');
                    if ($this->isAdvanced() || $this->isCitizenArrest()) {
                        $newState = $oldState | 2;
                        $action = clienttranslate('exonerates');
                        self::incStat(1, "suspectsExonerated", self::getGameStateValue('killer_bga_playerID'));
                    }
                    $newboard[$index + 1] = $newState;
                    self::DbQuery("UPDATE board SET kID='$card' LIMIT 1");
                    $beforeShrink = $newboard;
                    $shrinkDetail = $this->shrinkBoard($newboard);
                    $this->saveBoardAndDeck($newboard, $newDeck);
                    self::notifyPlayer(self::getCurrentPlayerId(), "updateID",
                        clienttranslate(""), 
                        array('newID' => $card, 'isKiller' => true));
                    self::notifyAllPlayers("killerIdChanged", "", array());
                    self::notifyAllPlayers("disguise",
                        clienttranslate('${player_name} disguises and ${action} ${oldId}'),
                        array('player_name' => self::getActivePlayerName(),
                            'action' => $action, 'oldId' => self::$charName[$gameData['kID']],
                            'newBoard' => $newboard, 'remainingDeckSize' => strlen($newDeck),
                            'beforeShrink' => $beforeShrink,
                            'shrinkDetail' => $shrinkDetail, 'i18n' => array('action')));
                    $this->notifyBoardShrink($shrinkDetail);
                    $this->gamestate->nextState('disguise');
                }
            }
        }
    }
    public function arrest($value) {
        self::checkAction('arrest');
        $this->executeArrest($value);
        $this->gamestate->nextState('arrest');
    }
    public function citizensArrest() {
        self::checkAction('citizensArrest');
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard, iHand iHand FROM board LIMIT 1");
        $hand = $gameData['iHand'];
        $handLen = strlen($hand);
        $aliveSuspectInHand = false;
        for ($i = 0 ; $i < $handLen; ++$i) {
            $char = $hand[$i];
            if ($this->isCharacterOnGameBoard($char, $gameData['gameBoard'])) {
                $aliveSuspectInHand = true;
                break;
            }
        }
        if ($aliveSuspectInHand){
            $this->gamestate->nextState('citizensArrest');
        } else {
            $message = clienttranslate("You must have a suspect card on the board in your hand to perform Citizen's Arrest.");
            self::notifyPlayer(self::getCurrentPlayerId(), "showError",
                '', array('message' => $message));
        }
    }
    public function pickCitizensArrestCard($value) {
        self::checkAction('pickCitizensArrestCard');
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard, iHand iHand, kID kID FROM board LIMIT 1");
        $newHand = implode('', explode($value, $gameData['iHand']));
        $newboard = $gameData['gameBoard'];
        $index = strpos($gameData['gameBoard'], $value);
        if ($index === FALSE) {
            // something went wrong!
        } else if (!$this->isCitizenArrest()) {
            self::notifyPlayer(self::getCurrentPlayerId(), "doNothing",
                clienttranslate("Citizen's arrest is not available in this mode."), array());
        } else if (!$this->isCharacterOnGameBoard($value, $gameData['gameBoard'])) {
            // client should block this action, something went wrong!
            $message = clienttranslate("You must pick a suspect on the board to perform Citizen's Arrest.");
            self::notifyPlayer(self::getCurrentPlayerId(), "showError",
                '', array('message' => $message));
            $this->gamestate->nextState('pickCitizensArrestCardInvalid');
        } else if (!$this->canCharacterPerformArrest($value, $gameData['gameBoard'])) {
            // selected suspect card can't perform Citizen's Arrest
            $message = clienttranslate("The suspect you picked can't perform Citizen's Arrest.");
            self::notifyPlayer(self::getCurrentPlayerId(), "showError",
                '', array('message' => $message));
            $this->gamestate->nextState('pickCitizensArrestCardInvalid');
        } else {
            $oldState = $newboard[$index + 1];
            $newboard[$index + 1] = $oldState | 2;
            $this->saveBoardAndHand($newboard, $newHand, '', $value);
            self::notifyPlayer(self::getCurrentPlayerId(), "updateHand", "", array('iHand' => $newHand));
            self::notifyAllPlayers("pickExonerate",
                clienttranslate('${player_name} exonerates ${target}'),
                array('player_name' => self::getActivePlayerName(),
                    'target' => self::$charName[$value],
                    'targetId' => $value,
                    'newBoard' => $newboard));
            self::incStat(1, "suspectsExonerated", self::getGameStateValue('inspector_bga_playerID'));
            $this->gamestate->nextState('pickCitizensArrestCard');
        }
    }
    public function pickCitizensArrestTarget($value) {
        self::checkAction('pickCitizensArrestTarget');
        $this->executeArrest($value);
        $this->gamestate->nextState('pickCitizensArrestTarget');
    }
    private function executeArrest($value) {
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard, kID kID FROM board LIMIT 1");
        $index = strpos($gameData['gameBoard'], $value);
        $oldState = $gameData['gameBoard'][$index + 1];
        $newboard = $gameData['gameBoard'];
        $newboard[$index + 1] = $oldState + 4;
        $this->saveBoard($newboard);
        self::incStat(1, "arrestAction", self::getGameStateValue('inspector_bga_playerID'));
        self::notifyAllPlayers("arrest",
            clienttranslate('${player_name} arrests ${target}'),
            array('player_name' => self::getActivePlayerName(),
                'target' => self::$charName[$value],
                'targetId' => $value,
                'isKiller' => ($gameData['kID'] == $value)));
        if ($gameData['kID'] != $value) {
            self::notifyAllPlayers("doNothing",
                clienttranslate('${target} is not the killer!'),
                array('target' => self::$charName[$value]));
        }
    }
    public function exonerate() {
        self::checkAction('exonerate');
        $gameData = self::getObjectFromDB("SELECT eviDeck deck, iHand iHand FROM board LIMIT 1");
        if (strlen($gameData['deck']) < 1) {
            // something went wrong!
        } else {
            $card = $gameData['deck'][0];
            $newDeck = substr($gameData['deck'], 1);
            $newHand = $gameData['iHand'] . $card;
            $this->saveDeckAndHand($newDeck, $newHand);
            self::notifyPlayer(self::getCurrentPlayerId(), "exonerate", "", array('newHand' => $newHand));
            self::notifyAllPlayers("updateEviDeckSize",
                clienttranslate('${player_name} draws a card from the evidence deck'),
                array('player_name' => self::getActivePlayerName(), 
                    'remainingDeckSize' => strlen($newDeck)));
            self::incStat(1, "exonerateAction", self::getGameStateValue('inspector_bga_playerID'));
            $this->gamestate->nextState('exonerate');
        }
    }
    public function pickExonerate($value) {
        self::checkAction('pickExonerate');
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard, iHand iHand, kID kID FROM board LIMIT 1");
        $newHand = implode('', explode($value, $gameData['iHand']));
        $newboard = $gameData['gameBoard'];
        $index = strpos($gameData['gameBoard'], $value);
        if ($index === FALSE) {
            // something went wrong!
        } else {
            $oldState = $newboard[$index + 1];
            $newboard[$index + 1] = $oldState | 2;
            $this->saveBoardAndHand($newboard, $newHand, '', $value);
            self::notifyPlayer(self::getCurrentPlayerId(), "updateHand", "", array('iHand' => $newHand));
            self::notifyAllPlayers("pickExonerate", 
                clienttranslate('${player_name} exonerates ${target}'),
                array('player_name' => self::getActivePlayerName(), 
                    'target' => self::$charName[$value],
                    'targetId' => $value, 
                    'newBoard' => $newboard));
            self::incStat(1, "suspectsExonerated", self::getGameStateValue('inspector_bga_playerID'));
            if (!$this->isClassic()) {
                $this->canvas($gameData, $value);
            }
            $this->gamestate->nextState('pickExonerate');
        }
    }
    private function canvas($gameData, $target) {
        if ($this->isTwoCharacterAdjacent($target, $gameData['kID'], $gameData['gameBoard'])) {
            self::notifyAllPlayers("canvasCharacter", 
                clienttranslate('The killer is adjacent to ${target}'), 
                array('target' => self::$charName[$target], 
                    'targetId' => $target,
                    'isAlert' => true));
        } else {
            self::notifyAllPlayers("canvasCharacter", 
                clienttranslate('The killer is not adjacent to ${target}'), 
                array('target' => self::$charName[$target], 
                    'targetId' => $target,
                    'isAlert' => false));
        }
    }
    public function performCanvas() {
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard, iHand iHand, kID kID, lastExonerated lastExonerated FROM board LIMIT 1");
        $this->canvas($gameData, $gameData['lastExonerated']);
        $this->gamestate->nextState('performCanvas');
    }
    private function saveBoardAndDeck($board, $deck, $forbiddenShift = '') {
        $sqlFormat = "UPDATE board SET gameBoard='%s', eviDeck='%s', forbiddenShift='%s' LIMIT 1";
        self::DbQuery(sprintf($sqlFormat, $board, $deck, $forbiddenShift));
        self::setStat(strlen($deck), 'remainingCards');
    }
    private function saveDeckAndHand($deck, $hand) {
        $sqlFormat = "UPDATE board SET eviDeck='%s', iHand='%s' LIMIT 1";
        self::DbQuery(sprintf($sqlFormat, $deck, $hand));
        self::setStat(strlen($deck), 'remainingCards');
    }
    private function saveBoardAndHand($board, $hand, $forbiddenShift = '', $lastExonerated = '') {
        $sqlFormat = "UPDATE board SET gameBoard='%s', iHand='%s', forbiddenShift='%s', lastExonerated = '%s' LIMIT 1";
        self::DbQuery(sprintf($sqlFormat, $board, $hand, $forbiddenShift, $lastExonerated));
    }
    private function saveBoard($board, $forbiddenShift = '') {
        $sqlFormat = "UPDATE board SET gameBoard='%s', forbiddenShift='%s' LIMIT 1";
        self::DbQuery(sprintf($sqlFormat, $board, $forbiddenShift));
    }
    private function saveDeck($deck) {
        $sqlFormat = "UPDATE board SET eviDeck='%s' LIMIT 1";
        self::DbQuery(sprintf($sqlFormat, $deck));
        self::setStat(strlen($deck), 'remainingCards');
    }
    //////////////////////////////////////////////////////////////////////////////
    //////////// Game state arguments
    ////////////
    /*
        Here, you can create methods defined as "game state arguments" (see "args" property in states.inc.php).
        These methods function is to return some additional information that is specific to the current
        game state.
    */
    /*
    
    Example for game state "MyGameState":
    
    function argMyGameState()
    {
        // Get some values from the current game situation in database...
    
        // return values:
        return array(
            'variable1' => $value1,
            'variable2' => $value2,
            ...
        );
    }    
    */
    //////////////////////////////////////////////////////////////////////////////
    //////////// Game state actions
    ////////////
    /*
        Here, you can create methods defined as "game state actions" (see "action" property in states.inc.php).
        The action method of state X is called everytime the current game state is set to X.
    */
    function stAfterFirstKill() {
        $player_id = self::activeNextPlayer();
        self::giveExtraTime($player_id);
        $this->gamestate->nextState('nextTurn');
    }
    function stAfterPickID() {
        $player_id = self::activeNextPlayer();
        self::giveExtraTime($player_id);
        $this->gamestate->nextState('nextTurn');
    }
    function stAfterKiller() {
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard, iID iID, kID kID FROM board LIMIT 1");
        $state = $gameData['gameBoard'][strpos($gameData['gameBoard'], $gameData['iID']) + 1];
        $numKilled = $this->countKilled($gameData['gameBoard']);
        self::setStat($numKilled, 'suspectsKilled', self::getGameStateValue('killer_bga_playerID'));
        if ($this->isKillled($state) || $numKilled >= 16) {
            self::DbQuery("UPDATE player SET player_score = 1 WHERE player_color = '000000'");
            if ($this->isKillled($state)) {
                self::notifyAllPlayers("doNothing", clienttranslate('The inspector is deseased. Game ends.'), array());
            } else {
                self::notifyAllPlayers("doNothing", clienttranslate('16 characters are deseased. Game ends.'), array());
            }
            self::notifyAllPlayers("doNothing",
                clienttranslate('The inspector\'s identity is: ${target}'), 
                array('target' => self::$charName[$gameData['iID']]));
            self::notifyAllPlayers("doNothing", 
                clienttranslate('The killer\'s identity is: ${target}'),
                array('target' => self::$charName[$gameData['kID']]));
            $player_id = self::activeNextPlayer();
            $this->gamestate->nextState('endGame');
        } else {
            $player_id = self::activeNextPlayer();
            self::giveExtraTime($player_id);
            $this->gamestate->nextState('nextTurn');
        }
    }
    function countKilled($board) {
        $killed = 0;
        $size = strlen($board);
        for ($i = 3;$i < $size;$i+= 2) {
            $state = $board[$i];
            if ($this->isKillled($state)) {
                $killed++;
            }
        }
        return $killed;
    }
    function stAfterInspector() {
        $gameData = self::getObjectFromDB("SELECT gameBoard gameBoard, kID kID, iID iID FROM board LIMIT 1");
        $state = $gameData['gameBoard'][strpos($gameData['gameBoard'], $gameData['kID']) + 1];
        if ($state >= 4) {
            self::DbQuery("UPDATE player SET player_score = 1 WHERE player_color = 'ffffff'");
            $this->saveBoard($this->cleanArrestState($gameData['gameBoard']));
            self::notifyAllPlayers("doNothing",
                clienttranslate('The killer is arrested. Game ends.'), array());
            self::notifyAllPlayers("doNothing",
                clienttranslate('The inspector\'s identity is: ${target}'),
                array('target' => self::$charName[$gameData['iID']]));
            self::notifyAllPlayers("doNothing",
                clienttranslate('The killer\'s identity is: ${target}'),
                array('target' => self::$charName[$gameData['kID']]));
            $player_id = self::activeNextPlayer();
            $this->gamestate->nextState('endGame');
        } else {
            $this->saveBoard($this->cleanArrestState($gameData['gameBoard']));
            $player_id = self::activeNextPlayer();
            self::giveExtraTime($player_id);
            $this->gamestate->nextState('nextTurn');
        }
    }
    function cleanArrestState($board) {
        $size = strlen($board);
        for ($i = 3;$i < $size;$i+= 2) {
            $state = $board[$i];
            if ($state >= 4) {
                $board[$i] = $state - 4;
            }
        }
        return $board;
    }
    //////////////////////////////////////////////////////////////////////////////
    //////////// Zombie
    ////////////
    /*
        zombieTurn:
        
        This method is called each time it is the turn of a player who has quit the game (= "zombie" player).
        You can do whatever you want in order to make sure the turn of this player ends appropriately
        (ex: pass).
    */
    function zombieTurn($state, $active_player) {
        $statename = $state['name'];
        if ($state['type'] == "activeplayer") {
            switch ($statename) {
                default:
                    $this->gamestate->nextState("zombiePass");
                break;
            }
            return;
        }
        if ($state['type'] == "multipleactiveplayer") {
            // Make sure player is in a non blocking status for role turn
            $sql = "
                UPDATE  player
                SET     player_is_multiactive = 0
                WHERE   player_id = $active_player
            ";
            self::DbQuery($sql);
            $this->gamestate->updateMultiactiveOrNextState('');
            return;
        }
        throw new feException("Zombie mode not supported at this game state: " . $statename);
    }
    ///////////////////////////////////////////////////////////////////////////////////:
    ////////// DB upgrade
    //////////
    function upgradeTableDb($from_version) {
        // $from_version is the current version of this game database, in numerical form.
        // For example, if the game was running with a release of your game named "140430-1345",
        // $from_version is equal to 1404301345
        if ($from_version <= 1704021447) {
            try {
                self::DbQuery("ALTER TABLE `board` ADD `lastExonerated` varchar(1) DEFAULT NULL");
                self::DbQuery("ALTER TABLE `zz_replay1_board` ADD `lastExonerated` varchar(1) DEFAULT NULL");
                self::DbQuery("ALTER TABLE `zz_replay2_board` ADD `lastExonerated` varchar(1) DEFAULT NULL");
                self::DbQuery("ALTER TABLE `zz_replay3_board` ADD `lastExonerated` varchar(1) DEFAULT NULL");
            }
            catch(feException $ex) {
                    // (ignore)
            }
        }
        if ($from_version <= 1507210858) {
            $gameData = self::getObjectFromDB("SELECT board board FROM board LIMIT 1");
            $board = $gameData['board'];
            try {
                self::DbQuery("ALTER TABLE `board` ADD `gameBoard` varchar(52) NOT NULL");
            }
            catch(feException $ex) {
                // (ignore)
            }
            try {
                self::DbQuery("ALTER TABLE `board` DROP COLUMN `board`");
            }
            catch(feException $ex) {
                // (ignore)
            }
            $sqlFormat = "UPDATE board SET gameBoard='%s' LIMIT 1";
            self::DbQuery(sprintf($sqlFormat, $board));
        }
    }
}
