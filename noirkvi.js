/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * noirkvi implementation : © CH Huang <chhuang76@gmail.com>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * noirkvi.js
 *
 * noirkvi user interface script
 * 
 * In this file, you are describing the logic of your user interface, in Javascript language.
 *
 */

define([
    "dojo","dojo/_base/declare",
    "ebg/core/gamegui",
    "ebg/counter"
],
function (dojo, declare) {
    return declare("bgagame.noirkvi", ebg.core.gamegui, {
        constructor: function(){
            console.log('noirkvi constructor');
            
            // Here, you can init the global variables of your user interface
            this.remainingDeckSize = 0;
            this.boardData = '';
            this.playerId = '';
            this.isKiller = false;
            this.forbiddenShift = '';
            this.lastExonerated = '';
            this.boardWidth = 5;
            this.boardHeight = 5;
            this.gameMode = 0;

            // Constants
            this.cardWidth = 72;
            this.cardHeight = 96;
            this.cardMargin = 0;
            this.arrowWidth = 48;
            this.arrowHeight = 48;
        },
        
        /*
            setup:
            
            This method must set up the game user interface according to current game situation specified
            in parameters.
            
            The method is called each time the game interface is displayed to a player, ie:
            _ when the game starts
            _ when a player refreshes the game page (F5)
            
            "gamedatas" argument contains all datas retrieved by your "getAllDatas" PHP method.
        */
        
        setup: function( gamedatas )
        {
            console.log( "Starting game setup" );

            // Setting up player boards
            for( var player_id in gamedatas.players )
            {
                var player = gamedatas.players[player_id];
            }

            // TODO: Set up your game interface here, according to "gamedatas"
            this.gameMode = gamedatas.gameMode;
            this.displayModeSpecificUi(this.gameMode);
            this.isKiller = false;
            if (!this.isSpectator) {
                this.isKiller = gamedatas.myRole.localeCompare('000000') == 0;
                this.displayPlayerRole(this.isKiller);
                this.displayPlayerId(gamedatas.gameBoard['id']);
                if (!this.isKiller) {
                    this.displayInspectorHand(gamedatas.gameBoard['hand']);
                } else {
                    dojo.style('deckAndIdContainer', 'top', '48px');
                }
            }

            this.populateGameBoard(gamedatas.gameBoard['gameBoard'],
                                   gamedatas.gameBoard['id'], this.isKiller);
            this.updateEviDeckSize(gamedatas.remainingDeckSize);
            this.setForbiddenShift(gamedatas.gameBoard['forbiddenShift']);
            this.setLastExonerated(gamedatas.gameBoard['lastExonerated']);
            if ((typeof gamedatas.gameBoard['id'] != 'undefined') &&
                (gamedatas.gameBoard['id'] != null)) {
                this.highLightMyId(gamedatas.gameBoard['id'], this.isKiller);
            }

            // Localize static strings
            $('eviCardsInYourHand').innerHTML = _("Evidence Cards in Your Hand");

            // Init tool tips
            this.initToolTips();

            // Setup game notifications to handle (see "setupNotifications" method below)
            this.setupNotifications();

            if (!this.isSpectator) {
                dojo.query( '.shiftArrow' ).connect( 'onclick', this, 'shift' );
                if ( this.isKiller ){
                    dojo.query( '.suspectCard' ).connect( 'onclick', this, 'kill' );
                    dojo.query( '#disguise' ).connect( 'onclick', this, 'disguise' );
                } else {
                    dojo.query( '.suspectCard' ).connect( 'onclick', this, 'arrest' );
                    dojo.query( '#exonerate' ).connect( 'onclick', this, 'exonerate' );
                    dojo.query( '#citizensArrest' ).connect( 'onclick', this, 'citizensArrest' );
                }
                dojo.query( '.pickID' ).connect( 'onclick', this, 'onEviCardClicked' );
                dojo.query( '.pickID' ).connect( 'onmouseenter', this, 'onEviCardMouseEnter' );
                dojo.query( '.pickID' ).connect( 'onmouseleave', this, 'onEviCardMouseLeave' );
    	    } else {
                // Hide buttons/zones for spectator
                dojo.style($('myRole'), 'display', 'none');
                dojo.style($('myIdContainer'), 'display', 'none');
                dojo.style($('disguise'), 'display', 'none');
                dojo.style($('exonerate'), 'display', 'none');
    	    }

            console.log( "Ending game setup" );
        },

        initToolTips: function()
        {
            this.addTooltipToClass( 'shiftArrow_Up', '', _('Shift this column.') );
            this.addTooltipToClass( 'shiftArrow_Down', '', _('Shift this column.') );
            this.addTooltipToClass( 'shiftArrow_Left', '', _('Shift this row.') );
            this.addTooltipToClass( 'shiftArrow_Right', '', _('Shift this row.') );
            this.addTooltip( 'disguise', '', _('Disguise yourself.') );
            this.addTooltip( 'exonerate', '', _('Exonerate a suspect') );
            this.addTooltip( 'numCharactersDeceased', _('If 16 characters are deceased at any time, the game ends and the Killer wins.'), '' );
            this.addTooltip( 'eviDeck', _('Remaining cards in the Evidence Deck.'), '' );
            this.setTooltipsToAdjacentSuspects(this.playerId);
        },

        displayPlayerRole: function( isKiller )
        {
            $('myRole').innerHTML = _("You are the ") + (isKiller? _("Killer") : _("Inspector"));
            if(isKiller) {
                dojo.style( 'inspectorsHand', 'display', 'none' );
                dojo.style( 'exonerate', 'display', 'none' );
            } else {
                dojo.style( 'disguise', 'display', 'none' );
            }
        },

        displayPlayerId: function( id )
        {
            if ( (typeof id != 'undefined') && (id != null) ) {
                $('myID').innerHTML = _("Your identity is:");
                $('myIDcard').className = 'eviCard eviCard_'+id;
                this.playerId = id;
            } else {
                $('myID').innerHTML = _("You have not picked your identity yet.");
            }
        },

        displayInspectorHand: function( hand ) 
        {
            var baseClass = 'pickID';
            for (i = 0; i < 4; i++) { 
                if(i < hand.length){
                    $('inspectorsEviCard_' + i).className = baseClass + ' eviCard_'+hand[i];
                    dojo.style( 'inspectorsEviCard_' + i, 'display', 'inline-block' );
                } else {
                    dojo.style( 'inspectorsEviCard_' + i, 'display', 'none' );
                }
            }
        },

        displayModeSpecificUi: function( gameMode )
        {
            if (gameMode != 3){
                dojo.style('citizensArrest', 'display', 'none');
            }
        },

        populateGameBoard: function ( board )
        {
            var oldBoard = this.boardData;
            this.boardData = board;
            for (x = 0; x < 5; x++) {
                for (y = 0; y < 5; y++) {
                    var index = y*10 + x*2 + 2;
                    var c = board[ index ];
                    var status = parseInt(board[ index + 1 ]);
                    $('suspectCard_'+x+'_'+y).className = 'suspectCard' + ' eviCard_' + c;

                    dojo.style( 'killedHighLight_'+x+'_'+y, 'display', 'none' );
                    dojo.style( 'innocentHighLight_'+x+'_'+y, 'display', 'none' );
                    if ( status % 2 == 1 ) { //status == 1 or 3
                        dojo.style( 'killedHighLight_'+x+'_'+y, 'display', 'inline-block' );
                    }
                    if ( status >= 2 ) {
                        dojo.style( 'innocentHighLight_'+x+'_'+y, 'display', 'inline-block' );
                    }
                }
            }

            if ( oldBoard != this.boardData ) {
                this.boardAnim(oldBoard, this.boardData);
            }

            this.boardWidth = board[0];
            this.boardHeight = board[1];
            this.hideDisabledLines(this.boardWidth, this.boardHeight);
            this.countKilled();
        },

        boardAnim: function ( oldBoard, newBoard )
        {
            var size = oldBoard.length;
            for (i = 2; i < size; i+=2) {
                if ( oldBoard[i] != newBoard[i] ) {
                    var oldCo = this.boardIndexToCo( i );
                    var newCo = this.boardIndexToCo( newBoard.indexOf(oldBoard[i]) );
                    var oldPos = this.boardCoToPos( oldCo );
                    var newPos = this.boardCoToPos( newCo );
                    var divName = "suspectCard_"+ newCo.x + '_' + newCo.y;
                    dojo.style( divName, {'left': oldPos.left+'px', 'top': oldPos.top+'px'});
                    this.slideToObjectPos( divName, "gameboard", newPos.left, newPos.top ).play();
                }
            }
        },

        hideDisabledLines: function ( width, height )
        {
            var arrowWidth = dojo.getStyle('shiftArrow_Up_0', 'width');
            var arrowHeight = dojo.getStyle('shiftArrow_Up_0', 'height');
            var cardWidth = dojo.getStyle('suspectCard_0_0', 'width');
            var cardHeight = dojo.getStyle('suspectCard_0_0', 'height');

            // vertical
            for (x = width; x < 5; x++) {
                for ( y = 0; y < 5; y++) {
                    dojo.style( 'suspectCard_'+x+'_'+y, 'display', 'none' );
                    dojo.style( 'shiftArrow_Right_'+y, 'left', arrowWidth + cardWidth*width + 'px');
                }

                dojo.style( 'shiftArrow_Up_'+x, 'display', 'none');
                dojo.style( 'shiftArrow_Down_'+x, 'display', 'none');
            }

            // horizontal
            for (y = height; y < 5; y++) {
                for ( x = 0; x < 5; x++) {
                    dojo.style( 'suspectCard_'+x+'_'+y, 'display', 'none' );
                    dojo.style( 'shiftArrow_Down_'+x, 'top', arrowHeight + cardHeight*height + 'px');
                }

                dojo.style( 'shiftArrow_Left_'+y, 'display', 'none');
                dojo.style( 'shiftArrow_Right_'+y, 'display', 'none');
            }

            dojo.style( 'numCharactersDeceased', 'top', this.arrowHeight*2 + height*this.cardHeight + 3 +'px');
        },

        countKilled: function ()
        {
            var killed = 0;
            var size = this.boardData.length;
            for ( var i = 3 ; i < size; i+=2) {
                var state = this.boardData[i];
                if( (state % 2) == 1) {
                    killed++;
                }
            }

            $('numCharactersDeceased').innerHTML = _("Number of characters deceased: ") + killed;
        },

        highLightMyId: function ( id, isKiller ) 
        {
            if (this.isSpectator) return;
            if ( (typeof id != 'undefined') && id != null && id.length > 0) {
                dojo.query( '.idHighLight' ).replaceClass( 'idHighLight' );
                var boardPos = this.findBoardCoById( id );

                $('idHighLight_'+boardPos.x+'_'+boardPos.y).className +=
                    ' idHighLight_'+ (isKiller?'Killer':'Inspector');
            }
        },

        highLightArrestedId: function ( id, isKiller )
        {
            if ((typeof id != 'undefined') && id != null && id.length > 0) {
                dojo.query( '.arrestHighLight' ).style( 'display', 'none');
                dojo.query( '.arrestHighLight' ).style( 'opacity', '100');

                var boardPos = this.findBoardCoById( id );
                dojo.style( 'arrestHighLight_'+boardPos.x+'_'+boardPos.y, 'display', 'inline-block'); 
                dojo.fadeOut( { node: 'arrestHighLight_'+boardPos.x+'_'+boardPos.y, delay: 1200 } ).play();

                var highLightType = isKiller ? "alertHighLight_": "questionHighLight_";
                var HLName = highLightType+boardPos.x+'_'+boardPos.y;
                dojo.style( HLName, 'opacity', '100');
                dojo.style( HLName, 'display', 'inline-block');
                dojo.fadeOut( { node: HLName, delay: 1500 } ).play();
            }
        },

        updateEviDeckSize: function ( size )
        {
            this.remainingDeckSize = size;
            $('eviDeck').innerHTML = size;
        },

        setForbiddenShift: function ( shift )
        {
            this.forbiddenShift = shift;
            dojo.query( '.shiftArrow' ).style( 'opacity', '1');
            if (shift.length > 0){
                dojo.style( 'shiftArrow_'+shift, 'opacity', '0.25');
            }
        },

        setTooltipsToAdjacentSuspects: function ( targetId )
        {
            if((typeof targetId != 'undefined') && targetId != null && targetId.length > 0) {
                var playerPos = this.findBoardCoById(targetId);
                for (boardx = 0; boardx < 5; boardx++) {
                    for (boardy = 0; boardy < 5; boardy++) {
                        var id = 'suspectCard_'+boardx+'_'+boardy;
                        var character = this.getSuspectCardId(dojo.byId(id));
                        if(this.isAdjacent(playerPos, {x:boardx, y:boardy}, !this.isKiller) && !this.isKilled(character)) {
                            this.addTooltip(id, '', this.isKiller ? _('Kill the suspect.') : _('Arrest the suspect.'));
                            dojo.style( id, 'cursor', 'pointer');
                        } else {
                            this.removeTooltip( id );
                            dojo.style( id, 'cursor', 'auto');
                        }
                    }
                }
            }
        },

        canvasCharacter: function ( id, isAlert )
        {
            var boardPos = this.findBoardCoById( id );
            var highLightType = isAlert ? "alertHighLight_": "questionHighLight_";

            var w = this.boardWidth;
            var h = this.boardHeight;
            if ((boardPos.x + 1 > w) || (boardPos.y + 1 > h)){
                return;
            }

            for (boardx = 0; boardx < w; boardx++) { 
                for (boardy = 0; boardy < h; boardy++) {
                    var character = this.getSuspectCardId( dojo.byId('suspectCard_'+boardx+'_'+boardy) );
                    if(this.isAdjacent(boardPos, {x:boardx, y:boardy}, false) && !this.isKilled(character) && !this.isInnocent(character)) {
                        var HLName = highLightType+boardx+'_'+boardy;
                        dojo.style( HLName, 'opacity', '100');
                        dojo.style( HLName, 'display', 'inline-block'); 
                        dojo.fadeOut( { node: HLName, delay: 1500 } ).play();
                    }
                }
            }
        },

        setLastExonerated: function ( id )
        {
            this.lastExonerated = id;
        },

        ///////////////////////////////////////////////////
        //// Game & client states
        
        // onEnteringState: this method is called each time we are entering into a new game state.
        //                  You can use this method to perform some user interface changes at this moment.
        //
        onEnteringState: function( stateName, args )
        {
            console.log( 'Entering state: '+stateName );
            
            switch( stateName )
            {
            case 'inspectorPickCitizensArrestTarget': // Only in Citizen Arrest game mode
                this.setTooltipsToAdjacentSuspects(this.lastExonerated);
                break;
            default:
                this.setTooltipsToAdjacentSuspects(this.playerId);
                break;
            }
        },

        // onLeavingState: this method is called each time we are leaving a game state.
        //                 You can use this method to perform some user interface changes at this moment.
        //
        onLeavingState: function( stateName )
        {
            console.log( 'Leaving state: '+stateName );
            
            switch( stateName )
            {
            case 'dummmy':
                break;
            }
        }, 

        // onUpdateActionButtons: in this method you can manage "action buttons" that are displayed in the
        //                        action status bar (ie: the HTML links in the status bar).
        // 
        onUpdateActionButtons: function( stateName, args )
        {
            console.log( 'onUpdateActionButtons: '+stateName );

            if( this.isCurrentPlayerActive() )
            {
                switch( stateName )
                {
/*
                Example:

                case 'myGameState':

                    // Add 3 action buttons in the action status bar:

                    this.addActionButton( 'button_1_id', _('Button 1 label'), 'onMyMethodToCall1' );
                    this.addActionButton( 'button_2_id', _('Button 2 label'), 'onMyMethodToCall2' );
                    this.addActionButton( 'button_3_id', _('Button 3 label'), 'onMyMethodToCall3' );
                    break;
*/
                }
            }
        },

        ///////////////////////////////////////////////////
        //// Utility methods

        getClickedSuspectCardPos: function ( evt )
        {
            var cid = evt.currentTarget.id.split('_');
            return {x:cid[1], y:cid[2]};
        },

        getClickedSuspectCardId: function ( evt ) //also works for evi cards
        {
            return this.getSuspectCardId(evt.currentTarget);
        },

        getSuspectCardId: function ( card )
        {
            var cname = card.className.split(' ');
            var cnameSplit = cname[1].split('_');
            var picked = cnameSplit[1];
            return picked;
        },

        isKilled: function ( id )
        {
            var status = this.getIdStatus(id);
            return (status % 2) == 1;
        },

        isInnocent: function ( id )
        {
            var status = this.getIdStatus(id);
            return status & 2;
        },

        getIdStatus: function ( id )
        {
            var index = this.boardData.indexOf(id);
            return this.boardData[ index + 1 ];
        },

        isAdjacent: function ( a, b, allowSamePoint )
        {
            if (!this.isOnBoard(a) || !this.isOnBoard(b)) {
                return false;
            }

            var xdif = Math.abs(a.x - b.x);
            if ( xdif != 0 && xdif != 1 ) {
                return false;
            }

            var ydif = Math.abs(a.y - b.y);
            if ( ydif != 0 && ydif != 1 ) {
                return false;
            }

            if ( xdif == 0 && ydif == 0 && !allowSamePoint ) {
                return false;
            }

            return true;
        },

        isOnBoard: function ( co )
        {
            if (co.x >= this.boardWidth || co.y >= this.boardHeight) {
                return false;
            }
            return true;
        },

        getClickedShiftArrowInfo: function ( evt )
        {
            var id = evt.currentTarget.id.split('_');
            return {type:id[1], index:id[2]}; 
        },

        boardIndexToCo: function ( index )
        {
            var i = (index - 2)/2;
            var coX = i % 5;
            var coY = Math.floor( i / 5 );
            return {x:coX, y:coY};
        },

        boardCoToPos: function ( co )
        {
            return {left: co.x * this.cardWidth + this.cardMargin + this.arrowWidth, top: co.y*this.cardHeight + this.cardMargin + this.arrowHeight};
        },

        findBoardCoById: function ( id )
        {
            for (boardx = 0; boardx < 5; boardx++) {
                for (boardy = 0; boardy < 5; boardy++) {
                    var className = $('suspectCard_'+boardx+'_'+boardy).className;
                    var classNameSplit = className.split(' ');
                    if(classNameSplit.length >= 2) {
                        var card = classNameSplit[1].split('_');
                        if(card[1] == id) {
                            return {x:boardx, y:boardy};
                        }
                    }
                }
            }
        },

        ///////////////////////////////////////////////////
        //// Player's action
        onEviCardClicked: function ( evt )
        {
            if( (typeof this.playerId != 'undefined') &&
                (this.playerId != null) &&
                (this.playerId.length > 0)) {

                // player ID is already picked, this card must be picked for exonerating or citizen's arrest
                var isCitizensArrest = this.checkAction( 'pickCitizensArrestCard', nomessage = true );
                if (isCitizensArrest) {
                    this.pickCitizensArrestCard( evt );
                } else {
                    this.pickExonerate( evt );
                }
            } else {
                // plyaer ID not picked yet, click for picking ID
                this.pickID( evt );
            }
        },

        onEviCardMouseEnter: function ( evt )
        {
            evt.preventDefault();
            dojo.stopEvent( evt );
            var picked = this.getClickedSuspectCardId(evt);
            var pos = this.findBoardCoById(picked);
            dojo.style( 'eviHighLight_'+pos.x+'_'+pos.y, 'display', 'inline-block');
        },

        onEviCardMouseLeave: function ( evt )
        {
            evt.preventDefault();
            dojo.stopEvent( evt );
            var picked = this.getClickedSuspectCardId(evt);
            var pos = this.findBoardCoById(picked);
            dojo.query( '.eviHighLight' ).style( 'display', 'none');
        },

        pickID: function ( evt )
        {
            evt.preventDefault();
            dojo.stopEvent( evt );
            if( this.checkAction( 'pickID' ) )
            {
                var picked = this.getClickedSuspectCardId(evt);
                if ( this.isKilled(picked) ) {
                    this.showMessage( _('You must not pick a deceased suspect.'), "error");
                } else {
                    this.ajaxcall( "/noirkvi/noirkvi/pickID.html", {id:picked}, this, function( result ) {} );
                    dojo.query( '.eviHighLight' ).style( 'display', 'none');
                }
            }
        },

        shift: function ( evt )
        {
            dojo.stopEvent( evt );
            if( this.checkAction( 'shift' ) )
            {
                var info = this.getClickedShiftArrowInfo(evt);
                if( this.forbiddenShift == (info.type + '_' + info.index) ) {
                    this.showMessage( _("You cannot use your shift to 'undo' the previous player's shift."), "error");
                } else {
                    this.ajaxcall( "/noirkvi/noirkvi/shift.html", info, this, function( result ) {} );
                }
            }
        },

        kill: function ( evt )
        {
            dojo.stopEvent( evt );
            if( this.checkAction( 'kill' ) )
            {
                var playerPos = this.findBoardCoById(this.playerId);
                var targetPos = this.getClickedSuspectCardPos(evt);
                if( this.isAdjacent(playerPos, targetPos, false) ) {
                    var picked = this.getClickedSuspectCardId(evt);
                    if ( this.isKilled(picked) ) {
                        this.showMessage( _('You must not pick a deceased suspect.'), "error");
                    } else {
                        this.ajaxcall( "/noirkvi/noirkvi/kill.html", {id:picked}, this, function( result ) {} );
                    }
                } else {
                    this.showMessage( _('You must pick a suspect who is adjacent to your character.'), "error");
                }
            }
        },

        disguise: function ( evt )
        {
            dojo.stopEvent( evt );
            if( this.checkAction( 'disguise' ) ) {
                if( this.remainingDeckSize > 0 ) {
                    this.ajaxcall( "/noirkvi/noirkvi/disguise.html", {}, this, function( result ) {} );
                } else {
                    this.showMessage( _('No more evidence card left.'), "error");
                }
            }
        },

        arrest: function ( evt )
        {
            dojo.stopEvent( evt );
            var isCitizensArrest = this.checkAction( 'pickCitizensArrestTarget', nomessage = true );
            if( isCitizensArrest || this.checkAction( 'arrest' ) )
            {
                var playerPos = this.findBoardCoById(
                    isCitizensArrest ? this.lastExonerated : this.playerId );
                var targetPos = this.getClickedSuspectCardPos(evt);
                if( this.isAdjacent(playerPos, targetPos, true) ) {
                    var picked = this.getClickedSuspectCardId(evt);
                    if ( this.isKilled(picked) ) {
                        this.showMessage( _('You must not pick a deceased suspect.'), "error");
                    } else if ( this.isInnocent(picked) ) {
                        this.showMessage( _('You must not arrest an innocent suspect.'), "error");
                    } else {
                        var action = isCitizensArrest ?
                            "/noirkvi/noirkvi/pickCitizensArrestTarget.html" :
                            "/noirkvi/noirkvi/arrest.html";
                        this.ajaxcall( action, {id:picked}, this, function( result ) {} );
                    }
                } else {
                    if ( isCitizensArrest ) {
                        this.showMessage( _('You must pick a suspect who is adjacent to newly exonerated character.'), "error");
                    } else {
                        this.showMessage( _('You must pick a suspect who is adjacent to your character, including yourself.'), "error");
                    }
                }
            }
        },

        exonerate: function ( evt )
        {
            dojo.stopEvent( evt );
            if( this.checkAction( 'exonerate' ) ) {
                if( this.remainingDeckSize > 0 ) {
                    this.ajaxcall( "/noirkvi/noirkvi/exonerate.html",
                                   {}, this, function( result ) {} );
                } else {
                    this.showMessage( _('No more evidence card left.'), "error");
                }
            }
        },

        pickExonerate: function ( evt )
        {
            evt.preventDefault();
            dojo.stopEvent( evt );
            if( this.checkAction( 'pickExonerate' ) ){
                var picked = this.getClickedSuspectCardId(evt);
                this.ajaxcall( "/noirkvi/noirkvi/pickExonerate.html",
                               {id:picked}, this, function( result ) {} );
            }
        },

        citizensArrest: function ( evt )
        {
            dojo.stopEvent( evt );
            if( this.checkAction( 'citizensArrest' ) ) {
                this.ajaxcall( "/noirkvi/noirkvi/citizensArrest.html",
                               {}, this, function ( result ) {} );
            }
        },

        pickCitizensArrestCard: function ( evt )
        {
            dojo.stopEvent( evt );
            if( this.checkAction( 'pickCitizensArrestCard' ) ) {
                var picked = this.getClickedSuspectCardId(evt);
                var pos = this.findBoardCoById(picked);
                if ( this.isOnBoard(pos) ){
                    this.ajaxcall( "/noirkvi/noirkvi/pickCitizensArrestCard.html",
                                   {id:picked}, this, function( result ) {} );
                } else {
                    this.showMessage( _("You must pick a suspect on game board to perform Citizen's Arrest."), "error");
                }
            }
        },

        ///////////////////////////////////////////////////
        //// Reaction to cometD notifications
        setupNotifications: function()
        {
            console.log( 'notifications subscriptions setup' );

            // TODO: here, associate your game notifications with local methods
            dojo.subscribe( 'pickID', this, "notif_pickID" );
            dojo.subscribe( 'updateID', this, "notif_updateID" );
            dojo.subscribe( 'updateHand', this, "notif_updateHand" );
            dojo.subscribe( 'updateEviDeckSize', this, "notif_updateEviDeckSize" );
            dojo.subscribe( 'shift', this, "notif_shift" );
            dojo.subscribe( 'kill', this, "notif_kill" );
            dojo.subscribe( 'disguise', this, "notif_disguise" );
            dojo.subscribe( 'killerIdChanged', this, "notif_killerIdChanged");
            dojo.subscribe( 'arrest', this, "notif_arrest" );
            dojo.subscribe( 'exonerate', this, "notif_exonerate" );
            dojo.subscribe( 'pickExonerate', this, "notif_pickExonerate" );
            dojo.subscribe( 'showError', this, "notif_showError" );
            dojo.subscribe( 'doNothing', this, "notif_doNothing" );
            dojo.subscribe( 'canvasCharacter', this, "notif_canvasCharacter" );
        },

        notif_pickID: function( notif )
        {

        },

        notif_updateID: function ( notif )
        {
            this.displayPlayerId(notif.args.newID);
            this.highLightMyId(notif.args.newID, this.isKiller);
        },

        notif_updateHand: function ( notif )
        {
            this.displayInspectorHand(notif.args.iHand);
        },

        notif_updateEviDeckSize: function ( notif )
        {
            this.updateEviDeckSize(notif.args.remainingDeckSize);
        },

        notif_shift: function( notif )
        {
            this.populateGameBoard(notif.args.newBoard);
            this.highLightMyId( this.playerId, this.isKiller);
            this.setForbiddenShift(notif.args.forbiddenShift);
            this.setLastExonerated('');
        },

        notif_kill: function( notif )
        {
            this.populateGameBoard(notif.args.newBoard);
            this.highLightMyId( this.playerId, this.isKiller);
            this.setForbiddenShift('');
        },

        notif_disguise: function( notif )
        {
           this.populateGameBoard(notif.args.newBoard);
           this.highLightMyId( this.playerId, this.isKiller);
           this.updateEviDeckSize(notif.args.remainingDeckSize);
           this.setForbiddenShift('');
        },

        notif_killerIdChanged: function( notif )
        {
            this.showMessage( _("The killer's identity has changed."), "info");
        },

        notif_arrest: function( notif )
        {
           this.showMessage( _("The inspector arrests a suspect."), "info");
           this.highLightArrestedId(notif.args.targetId, notif.args.isKiller);
           this.setForbiddenShift('');
           this.setLastExonerated('');
        },

        notif_exonerate: function( notif )
        {
           this.displayInspectorHand(notif.args.newHand);
           this.setLastExonerated('');
        },

        notif_pickExonerate: function( notif )
        {
           this.populateGameBoard(notif.args.newBoard);
           this.setForbiddenShift('');
           this.setLastExonerated(notif.args.targetId);
        },

        notif_canvasCharacter: function( notif )
        {
            this.canvasCharacter(notif.args.targetId, notif.args.isAlert);
            this.setLastExonerated('');
        },

        notif_showError: function( notif )
        {
            this.showMessage(notif.args.message, "error");
        },

        notif_doNothing: function( notif )
        {
            //do nothing
        }
   });
});
